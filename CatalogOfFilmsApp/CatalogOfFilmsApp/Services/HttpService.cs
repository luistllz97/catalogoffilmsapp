﻿using CatalogOfFilmsApp.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CatalogOfFilmsApp.Services
{
    public class HttpService
    {
        readonly string _baseUrl = "https://api.themoviedb.org/3/movie";

        readonly RestClient _client;
        public HttpService() => _client = new RestClient(_baseUrl);
        #region Catalogs
        public async Task<MoviesModel> GetTopRated()
        {
            RestRequest req = new RestRequest("/top_rated") { Method = Method.GET };
            req.AddQueryParameter("api_key", "b96692b8a87a22f93ed2f69e2d4baf03");
            req.AddHeader("Accept", "application/json");
            IRestResponse resp = await _client.ExecuteAsync(req);
            return resp.StatusCode == System.Net.HttpStatusCode.OK ? JsonConvert.DeserializeObject<MoviesModel>(resp.Content) : null;
        }
        public async Task<MoviesModel> GetUpcoming()
        {
            RestRequest req = new RestRequest("/upcoming") { Method = Method.GET };
            req.AddQueryParameter("api_key", "b96692b8a87a22f93ed2f69e2d4baf03");
            req.AddHeader("Accept", "application/json");
            IRestResponse resp = await _client.ExecuteAsync(req);
            return resp.StatusCode == System.Net.HttpStatusCode.OK ? JsonConvert.DeserializeObject<MoviesModel>(resp.Content) : null;
        }
        public async Task<MoviesModel> GetPopular()
        {
            RestRequest req = new RestRequest("/popular") { Method = Method.GET };
            req.AddQueryParameter("api_key", "b96692b8a87a22f93ed2f69e2d4baf03");
            req.AddHeader("Accept", "application/json");
            IRestResponse resp = await _client.ExecuteAsync(req);
            return resp.StatusCode == System.Net.HttpStatusCode.OK ? JsonConvert.DeserializeObject<MoviesModel>(resp.Content) : null;
        }
        #endregion
        #region Details
        public async Task<MovieModel> GetDetails(int MovieId)
        {
            RestRequest req = new RestRequest($"/{MovieId}") { Method = Method.GET };
            req.AddQueryParameter("api_key", "b96692b8a87a22f93ed2f69e2d4baf03");
            req.AddHeader("Accept", "application/json");
            IRestResponse resp = await _client.ExecuteAsync(req);
            return resp.StatusCode == System.Net.HttpStatusCode.OK ? JsonConvert.DeserializeObject<MovieModel>(resp.Content) : null;
        }

        public async Task<CreditsModel> GetCredits(int MovieId)
        {
            RestRequest req = new RestRequest($"/{MovieId}/credits") { Method = Method.GET };
            req.AddQueryParameter("api_key", "b96692b8a87a22f93ed2f69e2d4baf03");
            req.AddHeader("Accept", "application/json");
            IRestResponse resp = await _client.ExecuteAsync(req);
            return resp.StatusCode == System.Net.HttpStatusCode.OK ? JsonConvert.DeserializeObject<CreditsModel>(resp.Content) : null;
        }
        #endregion
    }
}
