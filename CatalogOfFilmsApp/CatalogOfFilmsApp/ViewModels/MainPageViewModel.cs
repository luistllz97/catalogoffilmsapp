﻿using CatalogOfFilmsApp.Models;
using CatalogOfFilmsApp.Services;
using CatalogOfFilmsApp.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CatalogOfFilmsApp.ViewModels
{
    public class MainPageViewModel : BindableObject
    {
        #region Props

        private readonly HttpService HttpService = new HttpService();
        public INavigation Navigation { get; set; }
        public static readonly BindableProperty _TopRated = BindableProperty.Create(nameof(TopRated), typeof(MoviesModel), typeof(MainPageViewModel), defaultBindingMode: BindingMode.TwoWay);
        public MoviesModel TopRated { get => (MoviesModel)GetValue(_TopRated); set => SetValue(_TopRated, value); }
        public static readonly BindableProperty _UpComing = BindableProperty.Create(nameof(UpComing), typeof(MoviesModel), typeof(MainPageViewModel), defaultBindingMode: BindingMode.TwoWay);
        public MoviesModel UpComing { get => (MoviesModel)GetValue(_UpComing); set => SetValue(_UpComing, value); }
        public static readonly BindableProperty _Popular = BindableProperty.Create(nameof(Popular), typeof(MoviesModel), typeof(MainPageViewModel), defaultBindingMode: BindingMode.TwoWay);
        public MoviesModel Popular { get => (MoviesModel)GetValue(_Popular); set => SetValue(_Popular, value); }

        public static readonly BindableProperty _IsFilter = BindableProperty.Create(nameof(IsFilter), typeof(bool), typeof(MainPageViewModel), false, defaultBindingMode: BindingMode.TwoWay);
        public bool IsFilter { get => (bool)GetValue(_IsFilter); set => SetValue(_IsFilter, value); }
        public static readonly BindableProperty _QueryString = BindableProperty.Create(nameof(QueryString), typeof(string), typeof(MainPageViewModel), defaultBindingMode: BindingMode.TwoWay);
        public string QueryString { get => (string)GetValue(_QueryString); set => SetValue(_QueryString, value); }

        public ObservableCollection<Result> UpComingMovies { get; set; } = new ObservableCollection<Result>();
        public ObservableCollection<Result> UpComingMoviesFilter { get; set; } = new ObservableCollection<Result>();
        public ObservableCollection<Result> TopMovies { get; set; } = new ObservableCollection<Result>();
        public ObservableCollection<Result> TopMoviesFilter { get; set; } = new ObservableCollection<Result>();
        public ObservableCollection<Result> PopualarsMovies { get; set; } = new ObservableCollection<Result>();
        public ObservableCollection<Result> PopualarsFilter { get; set; } = new ObservableCollection<Result>();



        #endregion
        #region Commands
        public Command<Result> ToDetailsCommand { get; set; }
        public Command FilterCommand { get; set; }
        public NavigationPage MainPage { get; private set; }
        #endregion

        public MainPageViewModel(INavigation navigation)
        {
            Navigation = navigation;
            Get();
            ToDetailsCommand = new Command<Result>(async (Result) => await ToDetails(Result));
            FilterCommand = new Command(Filter);
        }

        public void Filter()
        {
            if (!string.IsNullOrEmpty(QueryString) && QueryString.Length >= 3)
            {
                IsFilter = true;
                List<Result> Aux = UpComingMovies.Where(x => x.Title.ToUpper().Contains(QueryString.ToUpper())).OrderBy(x => x.Title).ToList();
                List<Result> Aux1 = TopMovies.Where(x => x.Title.ToUpper().Contains(QueryString.ToUpper())).OrderBy(x => x.Title).ToList();
                List<Result> Aux2 = PopualarsMovies.Where(x => x.Title.ToUpper().Contains(QueryString.ToUpper())).OrderBy(x => x.Title).ToList();

                UpComingMoviesFilter.Clear();
                foreach (var item in Aux)
                    UpComingMoviesFilter.Add(item);

                TopMoviesFilter.Clear();
                foreach (var item in Aux1)
                    TopMoviesFilter.Add(item);

                PopualarsFilter.Clear();
                foreach (var item in Aux2)
                    PopualarsFilter.Add(item);
            }
            else
                IsFilter = false;
        }

        public async Task ToDetails(Result result) =>
            await Navigation.PushAsync(new DetailsPage(result));

        public async void Get()
        {
            Task[] Tasks = new Task[3];
            Tasks[1] = TopRatedGet();
            Tasks[0] = PopularGet();
            Tasks[2] = UpcomingGet();
            await Task.WhenAll(Tasks);
        }
        #region Catalogs
        public async Task TopRatedGet()
        {
            int Cout;
            TopRated = await HttpService.GetTopRated();
            TopMovies.Clear();
            foreach (var item in TopRated.Results)
            {
                Cout = (int)(item.Vote_average / 2);
                switch (Cout)
                {
                    case 1:
                        item.Img1 = "Star.png";
                        item.Img2 = "EmptyStar.png";
                        item.Img3 = "EmptyStar.png";
                        item.Img4 = "EmptyStar.png";
                        item.Img5 = "EmptyStar.png";
                        break;
                    case 2:
                        item.Img1 = "Star.png";
                        item.Img2 = "Star.png";
                        item.Img3 = "EmptyStar.png";
                        item.Img4 = "EmptyStar.png";
                        item.Img5 = "EmptyStar.png";
                        break;
                    case 3:
                        item.Img1 = "Star.png";
                        item.Img2 = "Star.png";
                        item.Img3 = "Star.png";
                        item.Img4 = "EmptyStar.png";
                        item.Img5 = "EmptyStar.png";
                        break;
                    case 4:
                        item.Img1 = "Star.png";
                        item.Img2 = "Star.png";
                        item.Img3 = "Star.png";
                        item.Img4 = "Star.png";
                        item.Img5 = "EmptyStar.png";
                        break;
                    case 5:
                        item.Img1 = "Star.png";
                        item.Img2 = "Star.png";
                        item.Img3 = "Star.png";
                        item.Img4 = "Star.png";
                        item.Img5 = "Star.png";
                        break;
                    default:
                        break;
                }
                TopMovies.Add(item);
            }
        }
        public async Task PopularGet()
        {
            int Cout;
            Popular = await HttpService.GetPopular();
            PopualarsMovies.Clear();
            foreach (var item in Popular.Results)
            {
                Cout = (int)(item.Vote_average / 2);
                switch (Cout)
                {
                    case 1:
                        item.Img1 = "Star.png";
                        item.Img2 = "EmptyStar.png";
                        item.Img3 = "EmptyStar.png";
                        item.Img4 = "EmptyStar.png";
                        item.Img5 = "EmptyStar.png";
                        break;
                    case 2:
                        item.Img1 = "Star.png";
                        item.Img2 = "Star.png";
                        item.Img3 = "EmptyStar.png";
                        item.Img4 = "EmptyStar.png";
                        item.Img5 = "EmptyStar.png";
                        break;
                    case 3:
                        item.Img1 = "Star.png";
                        item.Img2 = "Star.png";
                        item.Img3 = "Star.png";
                        item.Img4 = "EmptyStar.png";
                        item.Img5 = "EmptyStar.png";
                        break;
                    case 4:
                        item.Img1 = "Star.png";
                        item.Img2 = "Star.png";
                        item.Img3 = "Star.png";
                        item.Img4 = "Star.png";
                        item.Img5 = "EmptyStar.png";
                        break;
                    case 5:
                        item.Img1 = "Star.png";
                        item.Img2 = "Star.png";
                        item.Img3 = "Star.png";
                        item.Img4 = "Star.png";
                        item.Img5 = "Star.png";
                        break;
                    default:
                        break;
                }
                PopualarsMovies.Add(item);
            }
        }
        public async Task UpcomingGet()
        {
            int Cout;
            UpComing = await HttpService.GetUpcoming();
            UpComingMovies.Clear();
            foreach (var item in UpComing.Results)
            {
                Cout = (int)(item.Vote_average / 2);
                switch (Cout)
                {
                    case 1:
                        item.Img1 = "Star.png";
                        item.Img2 = "EmptyStar.png";
                        item.Img3 = "EmptyStar.png";
                        item.Img4 = "EmptyStar.png";
                        item.Img5 = "EmptyStar.png";
                        break;
                    case 2:
                        item.Img1 = "Star.png";
                        item.Img2 = "Star.png";
                        item.Img3 = "EmptyStar.png";
                        item.Img4 = "EmptyStar.png";
                        item.Img5 = "EmptyStar.png";
                        break;
                    case 3:
                        item.Img1 = "Star.png";
                        item.Img2 = "Star.png";
                        item.Img3 = "Star.png";
                        item.Img4 = "EmptyStar.png";
                        item.Img5 = "EmptyStar.png";
                        break;
                    case 4:
                        item.Img1 = "Star.png";
                        item.Img2 = "Star.png";
                        item.Img3 = "Star.png";
                        item.Img4 = "Star.png";
                        item.Img5 = "EmptyStar.png";
                        break;
                    case 5:
                        item.Img1 = "Star.png";
                        item.Img2 = "Star.png";
                        item.Img3 = "Star.png";
                        item.Img4 = "Star.png";
                        item.Img5 = "Star.png";
                        break;
                    default:
                        break;
                }
                UpComingMovies.Add(item);
            }

        }
        #endregion
    }
}