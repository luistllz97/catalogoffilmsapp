﻿using CatalogOfFilmsApp.Models;
using CatalogOfFilmsApp.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace CatalogOfFilmsApp.ViewModels
{
    public class DetailsPageViewModel : BindableObject
    {
        #region Props
        public static readonly BindableProperty _Result = BindableProperty.Create(nameof(Result), typeof(Result), typeof(DetailsPageViewModel), defaultBindingMode: BindingMode.TwoWay);
        public Result Result { get => (Result)GetValue(_Result); set => SetValue(_Result, value); }
        public static readonly BindableProperty _Details = BindableProperty.Create(nameof(Details), typeof(MovieModel), typeof(DetailsPageViewModel), defaultBindingMode: BindingMode.TwoWay);
        public MovieModel Details { get => (MovieModel)GetValue(_Details); set => SetValue(_Details, value); }
        public static readonly BindableProperty _Credits = BindableProperty.Create(nameof(Credits), typeof(CreditsModel), typeof(DetailsPageViewModel), defaultBindingMode: BindingMode.TwoWay);
        public CreditsModel Credits { get => (CreditsModel)GetValue(_Credits); set => SetValue(_Credits, value); }
        public static readonly BindableProperty _GenresList = BindableProperty.Create(nameof(GenresList), typeof(string), typeof(DetailsPageViewModel), defaultBindingMode: BindingMode.TwoWay);
        public string GenresList { get => (string)GetValue(_GenresList); set => SetValue(_GenresList, value); }
        public static readonly BindableProperty _ReleaseDate = BindableProperty.Create(nameof(ReleaseDate), typeof(string), typeof(DetailsPageViewModel), defaultBindingMode: BindingMode.TwoWay);
        public string ReleaseDate { get => (string)GetValue(_ReleaseDate); set => SetValue(_ReleaseDate, value); }
        public ObservableCollection<Cast> CastList { get; set; } = new ObservableCollection<Cast>();
        private readonly HttpService HttpService = new HttpService();
        public INavigation Navigation { get; set; }
        #endregion
        #region Commands
        public Command BackCommand { get; set; }
        #endregion
        public DetailsPageViewModel(Result Model, INavigation navigation)
        {
            Navigation = navigation;
            Result = Model;
            BackCommand = new Command(async () => await Navigation.PopAsync());
            Get();
        }
        public async void Get()
        {
            Details = await HttpService.GetDetails(Result.Id);
            Credits = await HttpService.GetCredits(Result.Id);
            CastList.Clear();
            List<Cast> Aux = Credits.Cast.Take(10).ToList();
            foreach (var item in Aux)
                CastList.Add(item);
            GenresList = string.Join(", ", Details.Genres.Select(x => x.Name).ToArray());
            var AuxDate = Details.Release_date.Split('-');
            ReleaseDate = AuxDate[0];
        }
    }
}
