﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xamarin.Forms;

namespace CatalogOfFilmsApp.Models
{
    public class Result : BindableObject
    {
        public bool Adult { get; set; }
        public string Backdrop_path { get; set; }
        public List<int> Genre_ids { get; set; }
        public int Id { get; set; }
        public string Original_language { get; set; }
        public string Original_title { get; set; }
        public string Overview { get; set; }
        public double Popularity { get; set; }
        public string Poster_path { get; set; }
        public string Release_date { get; set; }
        public string Title { get; set; }
        public bool Video { get; set; }
        public double Vote_average { get; set; }
        public int Vote_count { get; set; }
        public string Image => "https://image.tmdb.org/t/p/w500/" + Poster_path;

        public static readonly BindableProperty _Img1 = BindableProperty.Create(nameof(Img1), typeof(string), typeof(Result), defaultBindingMode: BindingMode.TwoWay);
        public string Img1 { get => (string)GetValue(_Img1); set => SetValue(_Img1, value); }

        public static readonly BindableProperty _Img2 = BindableProperty.Create(nameof(Img1), typeof(string), typeof(Result), defaultBindingMode: BindingMode.TwoWay);
        public string Img2 { get => (string)GetValue(_Img2); set => SetValue(_Img2, value); }

        public static readonly BindableProperty _Img3 = BindableProperty.Create(nameof(Img1), typeof(string), typeof(Result), defaultBindingMode: BindingMode.TwoWay);
        public string Img3 { get => (string)GetValue(_Img3); set => SetValue(_Img3, value); }

        public static readonly BindableProperty _Img4 = BindableProperty.Create(nameof(Img1), typeof(string), typeof(Result), defaultBindingMode: BindingMode.TwoWay);
        public string Img4 { get => (string)GetValue(_Img4); set => SetValue(_Img4, value); }

        public static readonly BindableProperty _Img5 = BindableProperty.Create(nameof(Img1), typeof(string), typeof(Result), defaultBindingMode: BindingMode.TwoWay);
        public string Img5 { get => (string)GetValue(_Img5); set => SetValue(_Img5, value); }


    }
    public class StarsModel
    {
        public string Img { get; set; }
    }


    public class MoviesModel
    {
        public int Page { get; set; }
        public List<Result> Results { get; set; }
        public int Total_pages { get; set; }
        public int Total_results { get; set; }
    }





}
