﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CatalogOfFilmsApp.Models
{

    public class Genre
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class ProductionCompany
    {
        public int Id { get; set; }
        public string Logo_path { get; set; }
        public string Name { get; set; }
        public string Origin_country { get; set; }

        public string Image => "https://image.tmdb.org/t/p/w500" + Logo_path;
    }

    public class ProductionCountry
    {
        public string Iso_3166_1 { get; set; }
        public string Name { get; set; }
    }

    public class SpokenLanguage
    {
        public string English_name { get; set; }
        public string Iso_639_1 { get; set; }
        public string Name { get; set; }
    }

    public class MovieModel
    {
        public bool Adult { get; set; }
        public string Backdrop_path { get; set; }
        public object Belongs_to_collection { get; set; }
        public int Budget { get; set; }
        public List<Genre> Genres { get; set; }
        public string Homepage { get; set; }
        public int Id { get; set; }
        public string Imdb_id { get; set; }
        public string Original_language { get; set; }
        public string Original_title { get; set; }
        public string Overview { get; set; }
        public double Popularity { get; set; }
        public string Poster_path { get; set; }
        public List<ProductionCompany> Production_companies { get; set; }
        public List<ProductionCountry> Production_countries { get; set; }
        public string Release_date { get; set; }
        public int Revenue { get; set; }
        public int Runtime { get; set; }
        public List<SpokenLanguage> Spoken_languages { get; set; }
        public string Status { get; set; }
        public string Tagline { get; set; }
        public string Title { get; set; }
        public bool Video { get; set; }
        public double Vote_average { get; set; }
        public int Vote_count { get; set; }
        public string Image => "https://image.tmdb.org/t/p/original" + Poster_path;
        public string Image2 => "https://image.tmdb.org/t/p/original" + Backdrop_path;

    }



    public class Cast
    {
        public bool Adult { get; set; }
        public int Gender { get; set; }
        public int Id { get; set; }
        public string Known_for_department { get; set; }
        public string Name { get; set; }
        public string Original_name { get; set; }
        public double Popularity { get; set; }
        public string Profile_path { get; set; }
        public int Cast_id { get; set; }
        public string Character { get; set; }
        public string Credit_id { get; set; }
        public int Order { get; set; }
        public string Image => "https://image.tmdb.org/t/p/w500" + Profile_path;
    }

    public class Crew
    {
        public bool Adult { get; set; }
        public int Gender { get; set; }
        public int Id { get; set; }
        public string Known_for_department { get; set; }
        public string Name { get; set; }
        public string Original_name { get; set; }
        public double Popularity { get; set; }
        public string Profile_path { get; set; }
        public string Credit_id { get; set; }
        public string Department { get; set; }
        public string Job { get; set; }
        public string Image => "https://image.tmdb.org/t/p/w500" + Profile_path;

    }

    public class CreditsModel
    {
        public int Id { get; set; }
        public List<Cast> Cast { get; set; }
        public List<Crew> Crew { get; set; }
    }


}
